class Matrix:
    def __init__(self, inper):
        self.rows, self.columns = inper.split()

    def create(self):
        self.matrix = [[int(n) for n in input().split()] for _row in range(int(self.rows))]

    def penjumlahan(self, mtx):
        if self.rows == mtx.rows and self.columns == mtx.columns:
            result = [[self.matrix[i][j] + mtx.matrix[i][j] for j in range(len(self.matrix[0]))] for i in
                      range(len(self.matrix))]
            for row in result:
                for number in row:
                    print(number, end=" ")
                print()
        else:
            print("Terjadi kesalahan input. Silakan ulang kembali")
            
    def pengurangan(self, mtx):
        if self.rows == mtx.rows and self.columns == mtx.columns:
            result = [[self.matrix[i][j] - mtx.matrix[i][j] for j in range(len(self.matrix[0]))] for i in
                      range(len(self.matrix))]
            for row in result:
                for number in row:
                    print(number, end=" ")
                print()
        else:
            print("Terjadi kesalahan input. Silakan ulang kembali")
            

    def transpose(a,b):
        for i in range(n):
            for j in range(n):
                b[i][j] = a[j][i]
            
    
    def determinant(mtx):
        if len(mtx) == 1:
            return mtx[0][0]
        elif len(mtx) == 2:
            det = mtx[0][0] * mtx[1][1] - mtx[1][0] * mtx[0][1]
            return det
        else:
            recur = 0
            for i, e in enumerate(mtx):
                rex = mtx[0][i] * Matrix.determinant([[el for ind, el in enumerate(matx) if ind != i] for matx in mtx[1:]])
                if i % 2 == 0:
                    recur += rex
                else:
                    recur -= rex
            return recur
            
operasi = '''1. Penjumlahan
2. Pengurangan
3. Transpose
4. Determinan
5. Keluar'''

def menu():
    while True:
        print(operasi)
        pilihan = input()
        if pilihan == "1":
            print("Ukuran matriks: ")
            matrix_a = Matrix(input())
            print("Matriks pertama: ")
            matrix_a.create()

            print("Ukuran matriks: ")
            matrix_b = Matrix(input())
            print("Matriks kedua: ")
            matrix_b.create()

            print("Hasil dari operasi: ")
            matrix_a.penjumlahan(matrix_b)
        elif pilihan == "2":
            print("Ukuran matriks: ")
            matrix_a = Matrix(input())
            print("Matriks pertama: ")
            matrix_a.create()

            print("Ukuran matriks: ")
            matrix_b = Matrix(input())
            print("Matriks kedua: ")
            matrix_b.create()

            print("Hasil dari operasi: ")
            matrix_a.pengurangan(matrix_b)

       
        elif pilihan == "3":
            print("Ukuran matriks: ")
            a = Matrix(input())
            print("Matriks pertama: ")
            a.create()

            print("Ukuran matriks: ")
            b = Matrix(input())
            print("Matriks kedua: ")
            b.create()

            print("Hasil dari operasi: ")
            a.transpose(b)

            matrix_a.printer()
        elif choice == "4":
            print("Ukuran matriks: ")
            matrix_a = Matrix(input())
            print("Matrix: ")
            matrix_a.create()
            print("Hasil dari operasi: ")
            print(matrix_a.determinant(matrix_a.matrix))

            
        else:
            break


menu()
