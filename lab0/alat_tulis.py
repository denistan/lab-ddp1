harga_pulpen = 5000
harga_pensil = 4000
harga_cortape = 10000

print("Toko ini menjual:")
print("1. pulpen " + str(harga_pulpen) + "/pcs")
print("2. pensil " + str(harga_pensil) + "/pcs")
print("3. correction tape " + str(harga_cortape) + "/pcs")

print("Masukkan jumlah barang yang ingin anda beli")
jml_pulpen = int(input("Pulpen: "))
jml_pensil = int(input("Pensil: "))
jml_cortape = int(input("Correction tape: "))

total = (jml_pulpen * harga_pulpen) + (jml_pensil * harga_pensil) + (jml_cortape * harga_cortape)
print("Total Harga: " + str(total))

print("Ringkasan pembelian")
print(f"{jml_pulpen} pulpen x {harga_pulpen}")
print(f"{jml_pensil} pensil x {harga_pensil}")
print(f"{jml_cortape} correction tape x {harga_cortape}")