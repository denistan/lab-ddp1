class KueUlangTahun:
    def __init__(self, tipe, harga, tulisan, angka_lilin, topping):
        self.__tipe = tipe
        self.__harga = harga
        self.__tulisan = tulisan
        self.__angka_lilin = angka_lilin
        self.__topping = topping
    
    def get_tipe(self):
        # TODO: Implementasikan getter untuk tipe!
        tipe = input("Pilih tipe kue: ")
        pass

    def get_harga(self):
        # TODO: Implementasikan getter untuk harga!
        harga_1 = ¥2700
        harga_2 = ¥2200
        harga_3 = ¥3100
        pass
    
    def get_tulisan(self):
        # TODO: Implementasikan getter untuk tulisan!
        tulisan = input("Masukkan tulisan pada kue: ")        
        pass
    
    def get_angka_lilin(self):
        # TODO: Implementasikan getter untuk angka_lilin!
        angka_lilin = input("Masukkan angka lilin: ")
        pass
    
    def get_topping(self):
        # TODO: Implementasikan getter untuk topping!
        topping = input("Pilih topping (Ceri/Stroberi): ")
        pass
class KueSponge(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, rasa, warna_frosting, harga = 2500):
        # TODO: Implementasikan constructor untuk class ini!
        print(f"{get_tulisan}")
        print(f"{get_angka_lilin}")
        print(f"{get_topping}")
        print(f"{get_tipe()}")
        print(f"{get_harga}")
        pass
    
    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!
    
class KueKeju(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_keju, harga=3000):
        # TODO: Implementasikan constructor untuk class ini!
        print(f"{get_tulisan}")
        print(f"{get_angka_lilin}")
        print(f"{get_topping}")
        print(f"{get_tipe()}")
        print(f"{get_harga}")
        pass

    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!

class KueBuah(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_buah, harga=3500):
        # TODO: Implementasikan constructor untuk class ini!    
        print(f"{get_tulisan}")
        print(f"{get_angka_lilin}")
        print(f"{get_topping}")
        print(f"{get_tipe()}")
        print(f"{get_harga}")
        pass
    
    # TODO: Implementasikan getter untuk semua instance attribute tambahan di kelas ini!

# Fungsi ini harus diimplementasikan!
def buat_custom_bundle():
    # TODO: Implementasikan menu untuk membuat custom bundle!
    print("Jenis kue:")
    print("1. Kue Sponge")
    print("2. Kue Keju")
    print("3. Kue Buah")
    pass

# Fungsi ini harus diimplementasikan!
def pilih_premade_bundle():
    # TODO: Implementasikan menu untuk memilih premade bundle!
    print("Pilihan paket istimewa:")
    print("1. New York-style Cheesecake with Strawberry Topping")
    print("2. Chocolate Sponge Cake with Cherry Topping and Blue Icing")
    print("3. American Fruitcake with Apple-Grape-Melon-mix Topping")
    
    pass

# Fungsi ini harus diimplementasikan!
def print_detail_kue(kue):
    # TODO: Implementasikan kode untuk print detail dari suatu kue!
    print(f"{get_tipe()}")
    print(f"{get_harga}")
    print(f"{get_tulisan}")
    print(f"{get_angka_lilin}")
    print(f"{get_topping}")
    pass
# Fungsi main jangan diubah!
def main():
    print("Selamat datang di Homura!")
    print("Saat ini sedang diadakan event khusus bertema kue ulang tahun.")

    is_ganti = True

    while is_ganti:
        print("\nBundle kue yang kami sediakan: ")
        print("1. Bundle pre-made")
        print("2. Bundle custom\n")

        pilihan_bundle = input("Pilih bundle: ")

        kue = None

        while True:
            if pilihan_bundle == "1":
                kue = pilih_premade_bundle()
                break
            elif pilihan_bundle == "2":
                kue = buat_custom_bundle()
                break
            else:
                print("Pilihan anda tidak valid.")
                pilihan_bundle = input("Pilih paket: ")
        
        print("\nBerikut adalah kue pesanan anda: ")

        print_detail_kue(kue)

        while True:
            ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")

            if ganti == "Ya":
                break
            elif ganti == "Tidak":
                is_ganti = False
                break
            else:
                print("Pilihan anda tidak valid.")
                ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")
    
    print("\nTerima kasih sudah berbelanja di Homura!")

if __name__ == "__main__":
    main()
