def check_nth_element_of_list( x, L, n):
    if n == 0:
        return x == L[0]
    else:
        return check_nth_element_of_list(x, L[1:], n-1)

#task2
def member(x, L):
    cek = False
    for i in x:
        for j in L:
            if i == j:
                cek = True
                return cek
    return cek

#task3
def reverse(L1, L2, L3=[]):
    cek = False
    new_list = L1[::-1] 
    if new_list == L2:
        cek = True
        return cek
    return cek

#task4
def palindrome(L):
    cek = False
    if L == L[::-1]:
        cek = True
        return cek
    return cek

#task5
def list_to_set(List1, List2=[]):
    my_set = set(List1)
    cek = any(i in List2 for i in my_set)
    return cek
